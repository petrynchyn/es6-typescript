import { controls } from '../../constants/controls';
import { IFighter }from '../interfaces';

export async function fight(firstFighter: IFighter, secondFighter: IFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const {
      PlayerOneAttack: attack1F,
      PlayerOneBlock: block1F,
      PlayerTwoAttack: attack2F,
      PlayerTwoBlock: block2F,
      PlayerOneCriticalHitCombination: crHitComb1F,
      PlayerTwoCriticalHitCombination: crHitComb2F
    } = controls

    let { health: health1F } = firstFighter  
    let { health: health2F } = secondFighter

    let isBlocked1F = false
    let isBlocked2F = false

    let isAllowedCrHit1F = true
    let isPressedKey1CrHit1F = false
    let isPressedKey2CrHit1F = false
    let isPressedKey3CrHit1F = false

    let isAllowedCrHit2F = true
    let isPressedKey1CrHit2F = false
    let isPressedKey2CrHit2F = false
    let isPressedKey3CrHit2F = false

    let indicator1F = document.getElementById('left-fighter-indicator') as HTMLElement;
    let indicator2F = document.getElementById('right-fighter-indicator') as HTMLElement;

    let healthDamage = (damage = 0, is1F = true) => {

      if (is1F) {

        health1F = Math.max(0, health1F - damage)

        const indWidth = Math.max(0, health1F * 100 / firstFighter.health)
        indicator1F!.style.width = indWidth + '%'

        if ( !health1F ) {
          removeKeyEventListaners()
          resolve(secondFighter)
        }

      } else {

        health2F = Math.max(0, health2F - damage)

        const indWidth = health2F * 100 / secondFighter.health
        indicator2F!.style.width = indWidth + '%'

        if ( !health2F ) {
          removeKeyEventListaners()
          resolve(firstFighter)
        }
      }

    }

    let removeKeyEventListaners = () => {
      document.removeEventListener('keydown', handlerKeyDown)     
      document.removeEventListener('keyup', handlerKeyUp)
    }

    let handlerKeyDown = (event: KeyboardEvent) => {

      if ( event.repeat )  return;

      switch ( event.code ) {
        case block1F:
          isBlocked1F = true
          break
        
        case block2F:
          isBlocked2F = true
          break
        
      // PlayerOneCriticalHitCombination
        
        case crHitComb1F[0]:
          isPressedKey1CrHit1F = true
          break

        case crHitComb1F[1]:
          isPressedKey2CrHit1F = true
          break

        case crHitComb1F[2]:
          isPressedKey3CrHit1F = true
          break
        
      // PlayerTwoCriticalHitCombination
        
        case crHitComb2F[0]:
          isPressedKey1CrHit2F = true
          break

        case crHitComb2F[1]:
          isPressedKey2CrHit2F = true
          break

        case crHitComb2F[2]:
          isPressedKey3CrHit2F = true
          break
      }


      if ( !isBlocked1F && !isBlocked2F ) {
        
        if ( event.code === attack1F )  {

          healthDamage( getDamage(firstFighter, secondFighter), false )
          
        } else if ( event.code === attack2F ) {

          healthDamage( getDamage(secondFighter, firstFighter) )
          
        }

      }

      if ( isAllowedCrHit1F && !isBlocked1F && isPressedKey1CrHit1F && isPressedKey2CrHit1F && isPressedKey3CrHit1F ) {

        isAllowedCrHit1F = false
        setTimeout( () => { isAllowedCrHit1F = true }, 10000 )
        healthDamage( 2 * firstFighter.attack, false )
        
      }

      if ( isAllowedCrHit2F && !isBlocked2F && isPressedKey1CrHit2F && isPressedKey2CrHit2F && isPressedKey3CrHit2F ) {

        isAllowedCrHit2F = false
        setTimeout( () => { isAllowedCrHit2F = true }, 10000 )
        healthDamage( 2 * secondFighter.attack )
        
      }

    }

    let handlerKeyUp = (event: KeyboardEvent) => {

      switch ( event.code ) {
        case block1F:
          isBlocked1F = false
          break

        case block2F:
          isBlocked2F = false
          break

        case crHitComb1F[0]:
          isPressedKey1CrHit1F = false
          break

        case crHitComb1F[1]:
          isPressedKey2CrHit1F = false
          break

        case crHitComb1F[2]:
          isPressedKey3CrHit1F = false
          break

        case crHitComb2F[0]:
          isPressedKey1CrHit2F = false
          break

        case crHitComb2F[1]:
          isPressedKey2CrHit2F = false
          break

        case crHitComb2F[2]:
          isPressedKey3CrHit2F = false
          break
      }
    }

    document.addEventListener('keydown', handlerKeyDown)
    document.addEventListener('keyup', handlerKeyUp)

  });
}

export function getDamage(attacker: IFighter, defender: IFighter) {
  // return damage
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender))
}

export function getHitPower(fighter: IFighter) {
  // return hit power
  return fighter.attack * ( 1 + Math.random() )
}

export function getBlockPower(fighter: IFighter) {
  // return block power
  return fighter.defense * ( 1 + Math.random() )
}
