import { showModal } from "./modal"
import { createFighterImage } from "../fighterPreview"
import { renderArena } from '../arena';
import { Fighters, IFighter } from '../../interfaces';

export function showWinnerModal(winner: IFighter, selectedFighters: Fighters) {
  // call showModal function 

  const imgElement = createFighterImage(winner) as HTMLImageElement
  imgElement.style.width = "fit-content"
  imgElement.style.alignSelf = "center"

  showModal({title: `${winner.name.toUpperCase()} is WINNER!`, bodyElement: imgElement, onClose: () => renderArena(selectedFighters)})
}
