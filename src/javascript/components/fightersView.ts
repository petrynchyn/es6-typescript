import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { IFighter, Fighters } from '../interfaces';

export function createFighters(fighters: Fighters) {
  const selectFighter = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root' }) as HTMLElement;
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' }) as HTMLElement;
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' }) as HTMLElement;
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: IFighter, selectFighter: Function) {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' }) as HTMLElement;
  const imageElement = createImage(fighter) as HTMLElement;
  const onClick: EventListener = (event: Event) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: IFighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  }) as HTMLElement;

  return imgElement;
}