import { createElement } from '../helpers/domHelper';
import { IFighter } from '../interfaces';

export function createFighterPreview(fighter: IFighter, position: string) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const imgElement = createFighterImage(fighter);
    imgElement.style.maxWidth = "none"
    fighterElement.append(imgElement);
    
    const divElement = createElement({
      tagName: 'div',
      className: 'arena___fighter-name'
    });
    divElement.innerHTML = `<center>
      ${fighter.name}
      <br><small>health: ${fighter.health}
      <br>attack: ${fighter.attack}
      <br>defense: ${fighter.defense}
      </small></center>`;
    fighterElement.append(divElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter: IFighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
