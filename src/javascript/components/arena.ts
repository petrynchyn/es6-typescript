import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighter, Fighters }from '../interfaces';

export async function renderArena(selectedFighters: Fighters) {
  const root = document.getElementById('root') as HTMLElement;
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  // todo:
  // - start the fight
  // - when fight is finished show winner

  const winner = await fight(...selectedFighters) as IFighter;

  showWinnerModal(winner, selectedFighters);
}

function createArena(selectedFighters: Fighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighter, rightFighter: IFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' }) as HTMLElement;
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' }) as HTMLElement;
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left') as HTMLElement;
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right') as HTMLElement;

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighter, position: string) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighter, secondFighter: IFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` }) as HTMLElement;
  const firstFighterElement = createFighter(firstFighter, 'left') as HTMLElement;
  const secondFighterElement = createFighter(secondFighter, 'right') as HTMLElement;

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighter, position: string) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  }) as HTMLElement;

  fighterElement.append(imgElement);
  return fighterElement;
}
