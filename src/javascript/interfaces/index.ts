export interface IFighter {
  _id: string,
  name: string,
  health: number,
  attack: number,
  defense: number,
  source: string,
}

export type Fighters = [IFighter, IFighter];